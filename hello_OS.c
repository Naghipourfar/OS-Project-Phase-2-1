#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros


MODULE_LICENSE("Mohsen/Alireza");
MODULE_AUTHOR("Mohsen Naghipourfar && Alireza Vezvaei");
MODULE_DESCRIPTION("A Module which just says hello to the OS!");


static int __init hello_OS_init(void) {
    printk(KERN_INFO "Hello Dear OS! This is a module created by Mohsen && Alireza! :)) \n");
    return 0;    // Non-zero return means that the module couldn't be loaded.
}

static void __exit hello_OS_cleanup(void) {
    printk(KERN_INFO "GoodBye OS! You've removed our module from your kernel! :(( \n");
}

module_init(hello_OS_init);
module_exit(hello_OS_cleanup);
